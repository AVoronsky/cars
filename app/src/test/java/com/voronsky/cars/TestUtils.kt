package com.voronsky.cars

import org.mockito.Mockito

fun <T> anyNotNull(): T {
    Mockito.any<T>()
    return uninitialized()
}

@Suppress("UNCHECKED_CAST")
private fun <T> uninitialized(): T = null as T