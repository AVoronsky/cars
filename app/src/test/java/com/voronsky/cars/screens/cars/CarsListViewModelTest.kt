package com.voronsky.cars.screens.cars

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.voronsky.cars.common.CoroutinesDispatcher
import com.voronsky.cars.common.VMDataWrapper
import com.voronsky.cars.screens.cars.data.Car
import com.voronsky.cars.screens.cars.data.CarsListRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.ArgumentCaptor
import org.mockito.Mockito.*

class CarsListViewModelTest {

    @Rule
    @JvmField
    var rule: TestRule = InstantTaskExecutorRule()

    private val repository = mock(CarsListRepository::class.java)
    private val coroutines = CoroutinesDispatcher(Dispatchers.Unconfined)
    private lateinit var carsListViewModel: CarsListViewModel

    @Before
    fun before() {
        carsListViewModel = CarsListViewModel(repository, coroutines)
    }

    @Test
    @Suppress("UNCHECKED_CAST")
    fun `loading should be set to true and false successively`() {
        val observer = mock(Observer::class.java) as Observer<Boolean>
        carsListViewModel.loading.observeForever(observer)

        runBlocking { `when`(repository.getCars()).thenReturn(listOf()) }
        carsListViewModel.loadCars()
        val captor = ArgumentCaptor.forClass(Boolean::class.java)
        captor.run {
            verify(observer, times(2)).onChanged(capture())
            assertTrue(captor.allValues[0])
            assertFalse(captor.allValues[1])
        }
    }

    @Test
    @Suppress("UNCHECKED_CAST")
    fun `verify error posted to livedata in case of exception`() {
        val observer = mock(Observer::class.java) as Observer<VMDataWrapper<Any>>
        carsListViewModel.cars.observeForever(observer)

        runBlocking { `when`(repository.getCars()).thenThrow(NullPointerException("exception")) }

        carsListViewModel.loadCars()

        val wrapper = carsListViewModel.cars.value!!
        assertNull(wrapper.data)
        assertTrue(wrapper.throwable is NullPointerException)
    }

    @Test
    @Suppress("UNCHECKED_CAST")
    fun `verify data posted to livedata in case of success`() {
        val observer = mock(Observer::class.java) as Observer<VMDataWrapper<List<Car>>>
        carsListViewModel.cars.observeForever(observer)

        val expectedData = listOf(
            Car(
                "any", "data", "can", "be", "set", "here", "for", "this",
                "test"
            )
        )
        runBlocking { `when`(repository.getCars()).thenReturn(expectedData) }

        carsListViewModel.loadCars()

        val wrapper = carsListViewModel.cars.value!!
        assertArrayEquals(expectedData.toTypedArray(), wrapper.data!!.toTypedArray())
        assertNull(wrapper.throwable)
    }
}