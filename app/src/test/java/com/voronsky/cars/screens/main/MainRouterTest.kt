package com.voronsky.cars.screens.main

import com.voronsky.cars.anyNotNull
import com.voronsky.cars.screens.CarsRouter
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mockito.*

class MainRouterTest {

    @Test
    fun `showDispatch() calls show() of CarsRouter`() {
        val carsRouter = mock(CarsRouter::class.java)
        val mainRouter = MainActivityRouter(carsRouter)

        mainRouter.showDispatch()

        verify(carsRouter, times(1)).show(anyNotNull(), anyNotNull(), ArgumentMatchers.anyBoolean())
    }
}