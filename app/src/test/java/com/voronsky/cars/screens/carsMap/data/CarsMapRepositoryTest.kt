package com.voronsky.cars.screens.carsMap.data

import com.google.android.gms.maps.model.LatLng
import com.voronsky.cars.data.cars.CarDto
import com.voronsky.cars.data.cars.CarsRepository
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.`when`

class CarsMapRepositoryTest {

    private val carsRepository = Mockito.mock(CarsRepository::class.java)
    private val carsMapDataRepository = CarsMapDataRepository(carsRepository)

    @Test
    fun `dto to regular object mapper converts correct fields`() {
        val dtoCar = CarDto(
            "id", "modelId", "modelN", "name", "BMW", "super", "grey", "pop",
            "gasoline", 30.0, "auto", "as2131as", 103.0, 102.0, "clean", "ulr.com"
        )
        val mappedCar = mapToMapCar(dtoCar)

        Assert.assertEquals(dtoCar.make, mappedCar.name)
        Assert.assertEquals(dtoCar.modelName, mappedCar.model)

        val latLng = LatLng(dtoCar.latitude, dtoCar.longitude)
        Assert.assertEquals(latLng.latitude, mappedCar.latLng.latitude, 0.0)
        Assert.assertEquals(latLng.longitude, mappedCar.latLng.longitude, 0.0)
    }

    @Test
    fun `getMapCars() returns the correct list of MapCar objects`() {
        val input = listOf(
            CarDto(
                "id", "1", "2", "3", "4", "5", "6", "7", "8", 0.0, "9", "10", 10.0, 10.0,
                "11", "12"
            ),
            CarDto(
                "id2", "13", "14", "15", "16", "17", "18", "19", "20", 0.0, "21", "22", 10.0,
                10.0, "23", "24"
            )
        )
        runBlocking { `when`(carsRepository.getCars()).thenReturn(input) }
        val actual = runBlocking { carsMapDataRepository.getMapCars() }
        val expected = input.map { mapToMapCar(it) }
        Assert.assertArrayEquals(expected.toTypedArray(), actual.toTypedArray())
    }
}