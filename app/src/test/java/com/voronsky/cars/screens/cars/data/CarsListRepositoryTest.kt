package com.voronsky.cars.screens.cars.data

import com.voronsky.cars.data.cars.CarDto
import com.voronsky.cars.data.cars.CarsRepository
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Assert.assertEquals
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.`when`

class CarsListRepositoryTest {

    private val carsRepository = Mockito.mock(CarsRepository::class.java)
    private val carsListRepository: CarsListRepository = CarsListDataRepository(carsRepository)

    @Test
    fun `dto to regular object mapper converts correct fields`() {
        val dtoCar = CarDto(
            "id", "modelId", "modelN", "name", "BMW", "super", "grey", "pop",
            "gasoline", 30.0, "auto", "as2131as", 103.0, 102.0, "clean", "ulr.com"
        )
        val mappedCar = mapToCar(dtoCar)

        assertEquals(dtoCar.make, mappedCar.vendor)
        assertEquals(dtoCar.modelName, mappedCar.modelName)
        assertEquals(dtoCar.color, mappedCar.color)
        assertEquals(dtoCar.fuelType, mappedCar.fuelType)
        assertEquals(dtoCar.fuelLevel.toString(), mappedCar.fuelLevel)
        assertEquals(dtoCar.transmission, mappedCar.transmission)
        assertEquals(dtoCar.licensePlate, mappedCar.licensePlate)
        assertEquals(dtoCar.innerCleanliness, mappedCar.cleanliness)
        assertEquals(dtoCar.carImageUrl, mappedCar.carImageUrl)
    }

    @Test
    fun `getCars() returns the correct list of Car objects`() {
        val input = listOf(
            CarDto(
                "id", "1", "2", "3", "4", "5", "6", "7", "8", 0.0, "9", "10", 10.0, 10.0,
                "11", "12"
            ),
            CarDto(
                "id2", "13", "14", "15", "16", "17", "18", "19", "20", 0.0, "21", "22", 10.0,
                10.0, "23", "24"
            )
        )

        runBlocking { `when`(carsRepository.getCars()).thenReturn(input) }
        val actual = runBlocking { carsListRepository.getCars() }
        val expected = input.map { mapToCar(it) }
        Assert.assertArrayEquals(expected.toTypedArray(), actual.toTypedArray())
    }
}