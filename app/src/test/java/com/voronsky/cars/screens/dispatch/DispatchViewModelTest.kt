package com.voronsky.cars.screens.dispatch

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.voronsky.cars.common.network.Connection
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.ArgumentCaptor
import org.mockito.Mockito.*

class DispatchViewModelTest {

    @Rule
    @JvmField
    var rule: TestRule = InstantTaskExecutorRule()

    private val router = mock(DispatchRouter::class.java)
    private val connection = mock(Connection::class.java)

    private lateinit var dispatchViewModel: DispatchViewModel

    @Before
    fun before() {
        dispatchViewModel = DispatchViewModel(router, connection)
    }

    @Test
    fun `verify router's showCars() was called if connection returns true`() {
        `when`(connection.isNetworkConnected()).thenReturn(true)

        dispatchViewModel.showCars()

        verify(router, times(1)).showCars()
    }

    @Test
    fun `verify router's showCars() wasn't called if connection returns true`() {
        `when`(connection.isNetworkConnected()).thenReturn(false)

        dispatchViewModel.showCars()

        verify(router, times(0)).showCars()
    }

    @Test
    fun `verify router's showCarsMap() was called if connection returns true`() {
        `when`(connection.isNetworkConnected()).thenReturn(true)

        dispatchViewModel.showCarsMap()

        verify(router, times(1)).showCarsMap()
    }

    @Test
    fun `verify router's showCarsMap() wasn't called if connection returns true`() {
        `when`(connection.isNetworkConnected()).thenReturn(false)

        dispatchViewModel.showCarsMap()

        verify(router, times(0)).showCarsMap()
    }

    @Test
    fun `verify error passed to networkError livedata if no connection calling showCars()`() {
        verifyNetworkErrorLiveData(false, { dispatchViewModel.showCars() }, 1) {
            Assert.assertNotNull(it)
        }
    }

    @Test
    fun `verify no error passed to networkError livedata if has connection calling showCars()`() {
        verifyNetworkErrorLiveData(true, { dispatchViewModel.showCars() }, 0)
    }

    @Test
    fun `verify error passed to networkError livedata if no connection calling showCarsMap()`() {
        verifyNetworkErrorLiveData(false, { dispatchViewModel.showCarsMap() }, 1) {
            Assert.assertNotNull(it)
        }
    }

    @Test
    fun `verify no error passed to networkError livedata if has connection calling showCarsMap()`() {
        verifyNetworkErrorLiveData(true, { dispatchViewModel.showCarsMap() }, 0)
    }

    @Suppress("UNCHECKED_CAST")
    private fun verifyNetworkErrorLiveData(
        isConnected: Boolean,
        action: () -> Unit,
        times: Int,
        assertCaptor: (Any?) -> Unit = {}
    ) {
        val observer = mock(Observer::class.java) as Observer<Any>
        dispatchViewModel.networkError.observeForever(observer)


        `when`(connection.isNetworkConnected()).thenReturn(isConnected)
        action()

        if (times > 0) {
            val captor = ArgumentCaptor.forClass(Any::class.java)
            captor.run {
                verify(observer, times(times)).onChanged(capture())
                assertCaptor(captor.value)
            }
        } else {
            verify(observer, times(times)).onChanged(Any())
        }
    }
}