package com.voronsky.cars.screens.dispatch

import com.voronsky.cars.anyNotNull
import com.voronsky.cars.screens.CarsRouter
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mockito.*

class DispatchRouterTest {

    private val carsRouter = mock(CarsRouter::class.java)
    private val dispatchRouter = DispatchFragmentRouter(carsRouter)

    @Test
    fun `verify showCars() called show()`() {
        dispatchRouter.showCars()
        verify(carsRouter, times(1)).show(anyNotNull(), anyNotNull(), ArgumentMatchers.anyBoolean())
    }

    @Test
    fun `verify showCarsMap() called show()`() {
        dispatchRouter.showCarsMap()
        verify(carsRouter, times(1)).show(anyNotNull(), anyNotNull(), ArgumentMatchers.anyBoolean())
    }
}