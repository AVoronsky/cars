package com.voronsky.cars.screens.main

import org.junit.Test
import org.mockito.Mockito.*

class MainActivityViewModelTest {

    @Test
    fun `showDispatch() called router's showDispatch()`() {
        val router = mock(MainRouter::class.java)
        val vm = MainActivityViewModel(router)
        vm.showDispatch()
        verify(router, times(1)).showDispatch()
    }
}