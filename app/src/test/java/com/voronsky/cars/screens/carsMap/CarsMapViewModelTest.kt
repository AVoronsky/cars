package com.voronsky.cars.screens.carsMap

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.google.android.gms.maps.model.LatLng
import com.voronsky.cars.common.CoroutinesDispatcher
import com.voronsky.cars.common.VMDataWrapper
import com.voronsky.cars.screens.carsMap.data.CarsMapRepository
import com.voronsky.cars.screens.carsMap.data.MapCar
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.ArgumentCaptor
import org.mockito.Mockito.*

class CarsMapViewModelTest {

    @Rule
    @JvmField
    var rule: TestRule = InstantTaskExecutorRule()

    private val repository = mock(CarsMapRepository::class.java)
    private val coroutines = CoroutinesDispatcher(Dispatchers.Unconfined)
    private lateinit var carsMapViewModel: CarsMapViewModel

    @Before
    fun before() {
        carsMapViewModel = CarsMapViewModel(repository, coroutines)
    }

    @Test
    @Suppress("UNCHECKED_CAST")
    fun `loading should be set to true and false successively`() {
        val observer = mock(Observer::class.java) as Observer<Boolean>
        carsMapViewModel.loading.observeForever(observer)

        runBlocking { `when`(repository.getMapCars()).thenReturn(listOf()) }
        carsMapViewModel.loadCars()
        val captor = ArgumentCaptor.forClass(Boolean::class.java)
        captor.run {
            verify(observer, times(2)).onChanged(capture())
            Assert.assertTrue(captor.allValues[0])
            Assert.assertFalse(captor.allValues[1])
        }
    }

    @Test
    @Suppress("UNCHECKED_CAST")
    fun `verify error posted to livedata in case of exception`() {
        val observer = mock(Observer::class.java) as Observer<VMDataWrapper<Any>>
        carsMapViewModel.mapCars.observeForever(observer)

        runBlocking { `when`(repository.getMapCars()).thenThrow(NullPointerException("exception")) }

        carsMapViewModel.loadCars()

        val wrapper = carsMapViewModel.mapCars.value!!
        Assert.assertNull(wrapper.data)
        Assert.assertTrue(wrapper.throwable is NullPointerException)
    }

    @Test
    @Suppress("UNCHECKED_CAST")
    fun `verify data posted to livedata in case of success`() {
        val observer = mock(Observer::class.java) as Observer<VMDataWrapper<List<MapCar>>>
        carsMapViewModel.mapCars.observeForever(observer)

        val expectedData = listOf(MapCar("any", "data", LatLng(20.0, 30.0)))
        runBlocking { `when`(repository.getMapCars()).thenReturn(expectedData) }

        carsMapViewModel.loadCars()

        val wrapper = carsMapViewModel.mapCars.value!!
        Assert.assertArrayEquals(expectedData.toTypedArray(), wrapper.data!!.toTypedArray())
        Assert.assertNull(wrapper.throwable)
    }
}