package com.voronsky.cars.common.network

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import androidx.test.core.app.ApplicationProvider
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.robolectric.RobolectricTestRunner
import org.robolectric.Shadows

@RunWith(RobolectricTestRunner::class)
class ConnectionTest {

    private val context = ApplicationProvider.getApplicationContext<Context>()
    private val connection: Connection = NetworkConnection(context)
    private val shadowConnectivityManager = Shadows.shadowOf(
        context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    )

    @Test
    fun `isNetworkConnected() should return true if networkInfo is not null`() {
        val networkInfo = Mockito.mock(NetworkInfo::class.java)
        shadowConnectivityManager.setActiveNetworkInfo(networkInfo)
        Assert.assertTrue(connection.isNetworkConnected())
    }

    @Test
    fun `isNetworkConnected() should return false if networkInfo is null`() {
        shadowConnectivityManager.setActiveNetworkInfo(null)
        Assert.assertFalse(connection.isNetworkConnected())
    }
}