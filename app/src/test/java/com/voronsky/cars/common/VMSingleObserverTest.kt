package com.voronsky.cars.common

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

class VMSingleObserverTest {

    @Rule
    @JvmField
    var rule: TestRule = InstantTaskExecutorRule()

    private val liveData = MutableLiveData<Any?>()
    private var data: Any? = null
    private var throwable: Throwable? = null
    private lateinit var observer: VMSingleObserver<Any, Any?>

    @Before
    fun before() {
        val onSuccess: (Any) -> Unit = { data = it }
        val onError: (Throwable) -> Unit = { throwable = it }

        observer = VMSingleObserver(liveData, onSuccess, onError)
    }

    @Test
    fun `verify callbacks wasn't invoked if data is null`() {
        observer.onChanged(null)

        Assert.assertNull(data)
        Assert.assertNull(throwable)
    }

    @Test
    fun `verify success invoked if data is not null`() {
        observer.onChanged(VMDataWrapper(Any()))

        Assert.assertNotNull(data)
        Assert.assertNull(throwable)
    }

    @Test
    fun `verify error invoked if exception is not null`() {
        observer.onChanged(VMDataWrapper(throwable = NullPointerException("exception")))

        Assert.assertNotNull(throwable)
        Assert.assertNull(data)
    }
}