package com.voronsky.cars

import androidx.fragment.app.Fragment
import com.voronsky.cars.screens.CarsNavigationRouter
import com.voronsky.cars.screens.CarsRouter
import com.voronsky.cars.screens.main.MainActivity
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.*
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class CarsRouterTest {

    private val activity = mock(MainActivity::class.java)
    private val fragment = mock(Fragment::class.java)
    private val router: CarsRouter = CarsNavigationRouter(activity)

    @Test
    fun `show() should call Activity's show()`() {
        router.show(fragment, null, false)
        verify(activity, times(1)).show(fragment, null, false)
    }

    @Test
    fun `goBack() should call Activity's popBackStack()`() {
        router.goBack()
        verify(activity, times(1)).popBackStack()
    }
}