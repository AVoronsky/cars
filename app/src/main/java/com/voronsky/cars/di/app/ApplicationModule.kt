package com.voronsky.cars.di.app

import android.content.Context
import com.voronsky.cars.CarsApplication
import com.voronsky.cars.common.CoroutinesDispatcher
import com.voronsky.cars.di.ActivityScope
import com.voronsky.cars.screens.main.MainActivity
import com.voronsky.cars.screens.main.di.MainActivityModule
import dagger.Module
import dagger.Provides
import dagger.android.AndroidInjectionModule
import dagger.android.ContributesAndroidInjector
import javax.inject.Singleton

@Module(includes = [AndroidInjectionModule::class])
abstract class ApplicationModule {

    @Module
    companion object {
        @JvmStatic
        @Provides
        @Singleton
        fun provideApplicationContext(carsApplication: CarsApplication): Context = carsApplication

        @JvmStatic
        @Provides
        @Singleton
        fun coroutines() = CoroutinesDispatcher()
    }

    @ActivityScope
    @ContributesAndroidInjector(modules = [MainActivityModule::class])
    abstract fun bindMainActivity(): MainActivity
}