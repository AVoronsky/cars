package com.voronsky.cars.di.app

import com.voronsky.cars.CarsApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        ApplicationModule::class,
        NetworkingModule::class,
        RepositoryModule::class
    ]
)
interface ApplicationComponent : AndroidInjector<CarsApplication> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun app(carsApplication: CarsApplication): Builder

        fun build(): ApplicationComponent
    }
}