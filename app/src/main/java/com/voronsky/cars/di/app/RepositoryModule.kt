package com.voronsky.cars.di.app

import com.voronsky.cars.common.network.RestNetworking
import com.voronsky.cars.data.DataRepository
import com.voronsky.cars.data.Repository
import com.voronsky.cars.data.cars.CarsApi
import com.voronsky.cars.data.cars.CarsRepository
import com.voronsky.cars.data.cars.CarsRestRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {

    @Provides
    @Singleton
    fun providesCarsApi(restNetworking: RestNetworking): CarsApi {
        return restNetworking.create(CarsApi::class.java)
    }

    @Provides
    @Singleton
    fun providesCarsRepository(carsApi: CarsApi): CarsRepository {
        return CarsRestRepository(carsApi)
    }

    @Provides
    @Singleton
    fun providesRepository(carsRepository: CarsRepository): Repository {
        return DataRepository(carsRepository)
    }
}