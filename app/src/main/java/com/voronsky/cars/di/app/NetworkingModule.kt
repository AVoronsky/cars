package com.voronsky.cars.di.app

import android.content.Context
import com.google.gson.Gson
import com.voronsky.cars.BuildConfig
import com.voronsky.cars.common.network.Connection
import com.voronsky.cars.common.network.NetworkConnection
import com.voronsky.cars.common.network.RestNetworking
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class NetworkingModule {
    @Provides
    @Singleton
    fun provideGson() = Gson()

    @Provides
    @Singleton
    fun provideConnection(context: Context): Connection {
        return NetworkConnection(context)
    }

    @Provides
    @Singleton
    fun provideRestNetworking(gson: Gson): RestNetworking {
        return RestNetworking(BuildConfig.BASE_API_URL, gson)
    }
}