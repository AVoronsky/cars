package com.voronsky.cars.screens.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class MainActivityViewModelFactory(private val mainRouter: MainRouter) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MainActivityViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return MainActivityViewModel(mainRouter) as T
        }
        throw IllegalStateException(
            "Wrong view model. " +
                    "Expected: ${MainActivityViewModel::class.java.simpleName} " +
                    "Actual: ${modelClass.simpleName}"
        )
    }
}