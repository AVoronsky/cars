package com.voronsky.cars.screens.carsMap

import androidx.lifecycle.MutableLiveData
import com.voronsky.cars.common.CoroutinesDispatcher
import com.voronsky.cars.common.VMDataWrapper
import com.voronsky.cars.common.base.BaseViewModel
import com.voronsky.cars.screens.carsMap.data.CarsMapRepository
import com.voronsky.cars.screens.carsMap.data.MapCar

class CarsMapViewModel(
    private val repository: CarsMapRepository,
    private val coroutines: CoroutinesDispatcher
) : BaseViewModel() {

    val mapCars = MutableLiveData<VMDataWrapper<List<MapCar>>>()

    fun loadCars() {
        loading.value = true
        val job = coroutines.io({
            val data = VMDataWrapper(repository.getMapCars())
            mapCars.postValue(data)
        }, {
            mapCars.postValue(VMDataWrapper(throwable = it))
        }, {
            loading.postValue(false)
        })
        jobs.add(job)
    }
}