package com.voronsky.cars.screens.dispatch

import com.voronsky.cars.screens.CarsRouter
import com.voronsky.cars.screens.cars.CarsListFragment
import com.voronsky.cars.screens.carsMap.CarsMapFragment

interface DispatchRouter {
    fun showCars()

    fun showCarsMap()
}

class DispatchFragmentRouter(private val carsRouter: CarsRouter) : DispatchRouter {
    override fun showCars() {
        carsRouter.show(CarsListFragment.newInstance(), CarsListFragment::class.java.simpleName)
    }

    override fun showCarsMap() {
        carsRouter.show(CarsMapFragment.newInstance(), CarsMapFragment::class.java.simpleName)
    }
}