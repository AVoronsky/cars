package com.voronsky.cars.screens.carsMap

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.voronsky.cars.common.CoroutinesDispatcher
import com.voronsky.cars.screens.carsMap.data.CarsMapRepository

class CarsMapViewModelFactory(
    private val repository: CarsMapRepository,
    private val coroutines: CoroutinesDispatcher
) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(CarsMapViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return CarsMapViewModel(repository, coroutines) as T
        }
        throw IllegalStateException(
            "Wrong view model. " +
                    "Expected: ${CarsMapViewModel::class.java.simpleName} " +
                    "Actual: ${modelClass.simpleName}"
        )
    }
}