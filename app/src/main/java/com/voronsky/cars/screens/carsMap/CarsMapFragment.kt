package com.voronsky.cars.screens.carsMap

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.MarkerOptions
import com.voronsky.cars.R
import com.voronsky.cars.common.VMSingleObserver
import com.voronsky.cars.common.base.BaseFragment
import com.voronsky.cars.screens.carsMap.data.MapCar
import javax.inject.Inject


class CarsMapFragment : BaseFragment() {

    companion object {
        fun newInstance() = CarsMapFragment()
    }

    private lateinit var map: GoogleMap

    @Inject
    lateinit var viewModel: CarsMapViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_cars_map, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        observeUpdates()
    }

    override fun onDestroyView() {
        viewModel.onDestroyView()
        super.onDestroyView()
    }

    private fun initView() {
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync {
            map = it
            viewModel.loadCars()
        }
    }

    private fun observeUpdates() {
        viewModel.mapCars.observe(
            viewLifecycleOwner,
            VMSingleObserver(
                viewModel.mapCars,
                { addMarkers(it) },
                { showToast(R.string.error_getting_data) })
        )
    }

    private fun addMarkers(cars: List<MapCar>) {
        val bounds = LatLngBounds.Builder()
        for (car in cars) {
            val marketOptions =
                MarkerOptions().position(car.latLng).title(car.name).snippet(car.model)
            map.addMarker(marketOptions)
            bounds.include(car.latLng)
        }
        map.moveCamera(
            CameraUpdateFactory.newLatLngBounds(
                bounds.build(),
                resources.getDimensionPixelOffset(R.dimen.map_camera_zoom_offset)
            )
        )
    }
}