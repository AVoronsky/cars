package com.voronsky.cars.screens.cars.data

import com.voronsky.cars.data.cars.CarDto
import com.voronsky.cars.data.cars.CarsRepository

interface CarsListRepository {
    suspend fun getCars(): List<Car>
}

class CarsListDataRepository(private val carsRepository: CarsRepository) : CarsListRepository {
    override suspend fun getCars(): List<Car> {
        return carsRepository.getCars().map(::mapToCar)
    }
}

fun mapToCar(car: CarDto): Car {
    return Car(
        car.make,
        car.modelName,
        car.color,
        car.fuelType,
        car.fuelLevel.toString(),
        car.transmission,
        car.licensePlate,
        car.innerCleanliness,
        car.carImageUrl
    )
}

data class Car(
    val vendor: String,
    val modelName: String,
    val color: String,
    val fuelType: String,
    val fuelLevel: String,
    val transmission: String,
    val licensePlate: String,
    val cleanliness: String,
    val carImageUrl: String
)