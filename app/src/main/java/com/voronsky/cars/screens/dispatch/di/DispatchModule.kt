package com.voronsky.cars.screens.dispatch.di

import androidx.lifecycle.ViewModelProvider
import com.voronsky.cars.common.network.Connection
import com.voronsky.cars.di.FragmentScope
import com.voronsky.cars.screens.CarsRouter
import com.voronsky.cars.screens.dispatch.*
import dagger.Module
import dagger.Provides

@Module
class DispatchModule {

    @Provides
    @FragmentScope
    fun providesDispatchRouter(carsRouter: CarsRouter): DispatchRouter =
        DispatchFragmentRouter(carsRouter)

    @Provides
    @FragmentScope
    fun providesDispatchViewModelFactory(dispatchRouter: DispatchRouter, connection: Connection)
            : DispatchViewModelFactory {
        return DispatchViewModelFactory(dispatchRouter, connection)
    }

    @Provides
    @FragmentScope
    fun providesDispatchViewModel(
        dispatchFragment: DispatchFragment,
        dispatchViewModelFactory: DispatchViewModelFactory
    ): DispatchViewModel {
        return ViewModelProvider(
            dispatchFragment,
            dispatchViewModelFactory
        ).get(DispatchViewModel::class.java)
    }
}