package com.voronsky.cars.screens.carsMap.di

import androidx.lifecycle.ViewModelProvider
import com.voronsky.cars.common.CoroutinesDispatcher
import com.voronsky.cars.data.Repository
import com.voronsky.cars.di.FragmentScope
import com.voronsky.cars.screens.carsMap.CarsMapFragment
import com.voronsky.cars.screens.carsMap.CarsMapViewModel
import com.voronsky.cars.screens.carsMap.CarsMapViewModelFactory
import com.voronsky.cars.screens.carsMap.data.CarsMapDataRepository
import com.voronsky.cars.screens.carsMap.data.CarsMapRepository
import dagger.Module
import dagger.Provides

@Module
class CarsMapFragmentModule {

    @Provides
    @FragmentScope
    fun providesCarsMapRepository(repository: Repository): CarsMapRepository {
        return CarsMapDataRepository(repository.getCarsRepository())
    }

    @Provides
    @FragmentScope
    fun providesCarsMapViewModelFactory(
        repository: CarsMapRepository,
        coroutines: CoroutinesDispatcher
    ): CarsMapViewModelFactory {
        return CarsMapViewModelFactory(repository, coroutines)
    }

    @Provides
    @FragmentScope
    fun providesCarsMapViewModel(
        carsMapFragment: CarsMapFragment,
        carsMapViewModelFactory: CarsMapViewModelFactory
    ): CarsMapViewModel {
        return ViewModelProvider(carsMapFragment, carsMapViewModelFactory)
            .get(CarsMapViewModel::class.java)
    }
}
