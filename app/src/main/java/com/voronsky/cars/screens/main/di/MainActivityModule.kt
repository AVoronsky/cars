package com.voronsky.cars.screens.main.di

import androidx.lifecycle.ViewModelProvider
import com.voronsky.cars.di.ActivityScope
import com.voronsky.cars.di.FragmentScope
import com.voronsky.cars.screens.CarsNavigationRouter
import com.voronsky.cars.screens.CarsRouter
import com.voronsky.cars.screens.cars.CarsListFragment
import com.voronsky.cars.screens.cars.di.CarsFragmentModule
import com.voronsky.cars.screens.carsMap.CarsMapFragment
import com.voronsky.cars.screens.carsMap.di.CarsMapFragmentModule
import com.voronsky.cars.screens.dispatch.DispatchFragment
import com.voronsky.cars.screens.dispatch.di.DispatchModule
import com.voronsky.cars.screens.main.*
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector

@Module
abstract class MainActivityModule {

    @Module
    companion object {
        @JvmStatic
        @ActivityScope
        @Provides
        fun providesCarsRouter(mainActivity: MainActivity): CarsRouter =
            CarsNavigationRouter(mainActivity)

        @JvmStatic
        @ActivityScope
        @Provides
        fun providesMainRouter(carsRouter: CarsRouter): MainRouter = MainActivityRouter(carsRouter)

        @JvmStatic
        @ActivityScope
        @Provides
        fun providesMainActivityViewModelFactory(mainRouter: MainRouter) =
            MainActivityViewModelFactory(mainRouter)

        @JvmStatic
        @ActivityScope
        @Provides
        fun providesMainActivityViewModel(
            mainActivity: MainActivity,
            factory: MainActivityViewModelFactory
        )
                : MainActivityViewModel {
            return ViewModelProvider(mainActivity, factory).get(MainActivityViewModel::class.java)
        }
    }

    @FragmentScope
    @ContributesAndroidInjector(modules = [DispatchModule::class])
    abstract fun dispatchFragment(): DispatchFragment

    @FragmentScope
    @ContributesAndroidInjector(modules = [CarsFragmentModule::class])
    abstract fun carsFragment(): CarsListFragment

    @FragmentScope
    @ContributesAndroidInjector(modules = [CarsMapFragmentModule::class])
    abstract fun carsMapFragment(): CarsMapFragment
}