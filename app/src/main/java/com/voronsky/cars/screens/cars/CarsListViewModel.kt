package com.voronsky.cars.screens.cars

import androidx.lifecycle.MutableLiveData
import com.voronsky.cars.common.CoroutinesDispatcher
import com.voronsky.cars.common.VMDataWrapper
import com.voronsky.cars.common.base.BaseViewModel
import com.voronsky.cars.screens.cars.data.Car
import com.voronsky.cars.screens.cars.data.CarsListRepository

class CarsListViewModel(
    private val repository: CarsListRepository,
    private val coroutines: CoroutinesDispatcher
) : BaseViewModel() {

    val cars = MutableLiveData<VMDataWrapper<List<Car>>>()

    fun loadCars() {
        loading.value = true
        val job = coroutines.io({
            cars.postValue(VMDataWrapper(repository.getCars()))
        }, {
            cars.postValue(VMDataWrapper(throwable = it))
        }, {
            loading.postValue(false)
        })
        jobs.add(job)
    }
}