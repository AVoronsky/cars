package com.voronsky.cars.screens.main

import com.voronsky.cars.common.base.BaseViewModel

class MainActivityViewModel(private val router: MainRouter) : BaseViewModel() {

    fun showDispatch() {
        router.showDispatch()
    }
}