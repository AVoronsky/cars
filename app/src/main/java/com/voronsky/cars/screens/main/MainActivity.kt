package com.voronsky.cars.screens.main

import android.os.Bundle
import com.voronsky.cars.R
import com.voronsky.cars.common.base.BaseActivity
import javax.inject.Inject

class MainActivity : BaseActivity() {

    @Inject
    lateinit var viewModel: MainActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        viewModel.showDispatch()
    }

    override fun getFragmentContainerId() = R.id.flContainer

    override fun onDestroy() {
        viewModel.onDestroyView()
        super.onDestroy()
    }
}
