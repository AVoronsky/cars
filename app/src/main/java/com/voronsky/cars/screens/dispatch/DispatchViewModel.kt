package com.voronsky.cars.screens.dispatch

import androidx.lifecycle.MutableLiveData
import com.voronsky.cars.common.base.BaseViewModel
import com.voronsky.cars.common.network.Connection

class DispatchViewModel(
    private val router: DispatchRouter,
    private val connection: Connection
) : BaseViewModel() {

    val networkError = MutableLiveData<Any>()

    fun showCars() {
        route { router.showCars() }
    }

    fun showCarsMap() {
        route { router.showCarsMap() }
    }

    private fun route(route: () -> Unit) {
        if (connection.isNetworkConnected()) {
            route()
        } else {
            networkError.value = Any()
        }
    }
}