package com.voronsky.cars.screens.cars

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.voronsky.cars.common.CoroutinesDispatcher
import com.voronsky.cars.screens.cars.data.CarsListRepository

class CarsListViewModelFactory(
    private val repository: CarsListRepository,
    private val coroutines: CoroutinesDispatcher
) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(CarsListViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return CarsListViewModel(repository, coroutines) as T
        }
        throw IllegalStateException(
            "Wrong view model. " +
                    "Expected: ${CarsListViewModel::class.java.simpleName} " +
                    "Actual: ${modelClass.simpleName}"
        )
    }
}