package com.voronsky.cars.screens

import androidx.fragment.app.Fragment
import com.voronsky.cars.common.base.BaseActivity

interface CarsRouter {
    fun show(fragment: Fragment, backStackTag: String? = null, isAnimated: Boolean = false)

    fun goBack()
}

class CarsNavigationRouter(private val baseActivity: BaseActivity) : CarsRouter {

    override fun show(fragment: Fragment, backStackTag: String?, isAnimated: Boolean) {
        baseActivity.show(fragment, backStackTag, isAnimated)
    }

    override fun goBack() {
        baseActivity.popBackStack()
    }
}