package com.voronsky.cars.screens.cars.di

import androidx.lifecycle.ViewModelProvider
import com.voronsky.cars.common.CoroutinesDispatcher
import com.voronsky.cars.data.Repository
import com.voronsky.cars.di.FragmentScope
import com.voronsky.cars.screens.cars.CarsListFragment
import com.voronsky.cars.screens.cars.CarsListViewModel
import com.voronsky.cars.screens.cars.CarsListViewModelFactory
import com.voronsky.cars.screens.cars.data.CarsListDataRepository
import com.voronsky.cars.screens.cars.data.CarsListRepository
import dagger.Module
import dagger.Provides

@Module
class CarsFragmentModule {

    @Provides
    @FragmentScope
    fun providesCarsListRepository(repository: Repository): CarsListRepository {
        return CarsListDataRepository(repository.getCarsRepository())
    }

    @Provides
    @FragmentScope
    fun providesCarsListViewModelFactory(
        repository: CarsListRepository,
        coroutines: CoroutinesDispatcher
    ): CarsListViewModelFactory {
        return CarsListViewModelFactory(repository, coroutines)
    }

    @Provides
    @FragmentScope
    fun providesCarsListViewModel(
        carsListFragment: CarsListFragment,
        carsListViewModelFactory: CarsListViewModelFactory
    ): CarsListViewModel {
        return ViewModelProvider(carsListFragment, carsListViewModelFactory)
            .get(CarsListViewModel::class.java)
    }
}