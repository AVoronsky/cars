package com.voronsky.cars.screens.cars.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.voronsky.cars.R
import com.voronsky.cars.common.utils.loadImage
import com.voronsky.cars.screens.cars.data.Car
import kotlinx.android.synthetic.main.item_car.view.*

class CarsAdapter : RecyclerView.Adapter<CarsAdapter.ViewHolder>() {

    var items = listOf<Car>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_car, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    inner class ViewHolder(private val item: View) : RecyclerView.ViewHolder(item) {
        fun bind(car: Car) {
            item.ivCarPhoto.loadImage(car.carImageUrl)
            item.tvGeneral.text = String.format(
                item.resources.getString(R.string.cars_list_car_general_pattern),
                car.vendor, car.modelName
            )
            item.tvColor.text = car.color
            item.tvFuelTypeLevel.text = String.format(
                item.resources.getString(R.string.cars_list_car_fuel_type_level_pattern),
                car.fuelType, car.fuelLevel
            )
            item.tvTransmission.text = car.transmission
            item.tvLicensePlate.text = car.licensePlate
            item.tvCleanliness.text = car.cleanliness
        }
    }
}
