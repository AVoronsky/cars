package com.voronsky.cars.screens.carsMap.data

import com.google.android.gms.maps.model.LatLng
import com.voronsky.cars.data.cars.CarDto
import com.voronsky.cars.data.cars.CarsRepository

interface CarsMapRepository {
    suspend fun getMapCars(): List<MapCar>
}

class CarsMapDataRepository(private val repository: CarsRepository) : CarsMapRepository {
    override suspend fun getMapCars(): List<MapCar> {
        return repository.getCars().map(::mapToMapCar)
    }
}

fun mapToMapCar(car: CarDto): MapCar {
    return MapCar(car.make, car.modelName, LatLng(car.latitude, car.longitude))
}

data class MapCar(
    val name: String,
    val model: String,
    val latLng: LatLng
)