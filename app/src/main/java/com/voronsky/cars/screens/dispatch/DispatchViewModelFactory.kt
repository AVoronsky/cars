package com.voronsky.cars.screens.dispatch

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.voronsky.cars.common.network.Connection

class DispatchViewModelFactory(
    private val router: DispatchRouter,
    private val connection: Connection
) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(DispatchViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return DispatchViewModel(router, connection) as T
        }
        throw IllegalStateException(
            "Wrong view model. " +
                    "Expected: ${DispatchViewModel::class.java.simpleName} " +
                    "Actual: ${modelClass.simpleName}"
        )
    }
}