package com.voronsky.cars.screens.main

import com.voronsky.cars.screens.CarsRouter
import com.voronsky.cars.screens.dispatch.DispatchFragment

interface MainRouter {

    fun showDispatch()
}

class MainActivityRouter(private val carsRouter: CarsRouter) : MainRouter {
    override fun showDispatch() {
        carsRouter.show(DispatchFragment.newInstance())
    }
}