package com.voronsky.cars.screens.cars

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.voronsky.cars.R
import com.voronsky.cars.common.VMSingleObserver
import com.voronsky.cars.common.base.BaseFragment
import com.voronsky.cars.common.utils.VerticalSpaceItemDecoration
import com.voronsky.cars.screens.cars.adapter.CarsAdapter
import kotlinx.android.synthetic.main.fragment_cars.*
import javax.inject.Inject

class CarsListFragment : BaseFragment() {

    companion object {
        fun newInstance() = CarsListFragment()
    }

    private val adapter = CarsAdapter()

    @Inject
    lateinit var viewModel: CarsListViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_cars, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        observeUpdates()
        viewModel.loadCars()
    }

    override fun onDestroyView() {
        viewModel.onDestroyView()
        super.onDestroyView()
    }

    private fun initViews() {
        rvCars.layoutManager = LinearLayoutManager(safeContext, RecyclerView.VERTICAL, false)
        rvCars.addItemDecoration(VerticalSpaceItemDecoration(resources.getDimensionPixelOffset(R.dimen.cars_list_items_padding)))
        rvCars.adapter = adapter
    }

    private fun observeUpdates() {
        viewModel.loading.observe(viewLifecycleOwner, getLoadingObserver())
        viewModel.cars.observe(viewLifecycleOwner, VMSingleObserver(viewModel.cars,
            { adapter.items = it }, { showToast(R.string.error_getting_data) })
        )
    }
}