package com.voronsky.cars.screens.dispatch

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.voronsky.cars.R
import com.voronsky.cars.common.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_dispatch.*
import javax.inject.Inject

class DispatchFragment : BaseFragment() {

    companion object {
        fun newInstance() = DispatchFragment()
    }

    @Inject
    lateinit var viewModel: DispatchViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_dispatch, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnCarsList.setOnClickListener { viewModel.showCars() }
        btnCarsMap.setOnClickListener { viewModel.showCarsMap() }
        viewModel.networkError.observe(
            viewLifecycleOwner,
            Observer { showToast(R.string.error_no_connection) })
    }

    override fun onDestroyView() {
        viewModel.onDestroyView()
        super.onDestroyView()
    }
}