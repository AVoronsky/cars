package com.voronsky.cars.common.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.Job

abstract class BaseViewModel : ViewModel() {

    protected val jobs = mutableListOf<Job>()
    val loading = MutableLiveData<Boolean>()

    open fun onDestroyView() {
        for (job in jobs) {
            job.cancel()
        }
    }
}