package com.voronsky.cars.common

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer

/**
 * Uses to avoid firing of the last produced item.
 * Use case: was shown the result of long running operation (like dialog "Download has been finished")
 * that should be shown only once and after it shouldn't appear.
 */
class VMSingleObserver<T, J>(
    private val liveData: MutableLiveData<J>,
    private val onSuccess: (T) -> Unit,
    private val onError: (Throwable) -> Unit
) : Observer<VMDataWrapper<T>> {

    override fun onChanged(t: VMDataWrapper<T>?) {
        if (t != null) {
            if (t.throwable == null) {
                onSuccess(t.data!!)
            } else {
                onError(t.throwable)
            }
            liveData.postValue(null)
        }
    }
}