package com.voronsky.cars.common.network

import android.content.Context
import android.net.ConnectivityManager

interface Connection {
    /**
     * Provides an information about network connectivity.
     *
     * @return - true if connected, otherwise false
     */
    fun isNetworkConnected(): Boolean
}

class NetworkConnection(private val context: Context) : Connection {

    override fun isNetworkConnected(): Boolean {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?

        return cm!!.activeNetworkInfo != null
    }
}