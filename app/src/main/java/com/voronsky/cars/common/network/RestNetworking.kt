package com.voronsky.cars.common.network

import com.google.gson.Gson
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class RestNetworking(baseUrl: String, private val gson: Gson) {

    private val okHttpClient: OkHttpClient
    private val retrofit: Retrofit

    init {
        okHttpClient = getHttpClientBuilder().build()
        retrofit = getRetrofit(okHttpClient, baseUrl)
    }

    fun <T> create(clazz: Class<T>): T {
        return retrofit.create(clazz)
    }

    fun <T> create(clazz: Class<T>, baseUrl: String): T {
        return create(clazz, okHttpClient, baseUrl)
    }

    private fun <T> create(clazz: Class<T>, okHttpClient: OkHttpClient, baseUrl: String): T {
        return getRetrofit(okHttpClient, baseUrl).create(clazz)
    }

    private fun getRetrofit(okHttpClient: OkHttpClient, baseUrl: String): Retrofit {
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpClient)
            .build()
    }

    private fun getHttpClientBuilder(): OkHttpClient.Builder {
        val logger = HttpLoggingInterceptor()
        logger.level = HttpLoggingInterceptor.Level.BODY

        return OkHttpClient.Builder()
            .addInterceptor(logger)
    }
}
