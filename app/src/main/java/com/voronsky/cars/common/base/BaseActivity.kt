package com.voronsky.cars.common.base

import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.voronsky.cars.common.views.LoadingDialogFragment
import dagger.android.support.DaggerAppCompatActivity

abstract class BaseActivity : DaggerAppCompatActivity() {

    private val loadingDialog = LoadingDialogFragment()
    private var isLoadingDialogDisplaying = false

    abstract fun getFragmentContainerId(): Int

    open fun buildTransaction(
        fragment: Fragment,
        backStackTag: String?,
        isAnimated: Boolean
    ): FragmentTransaction {
        val transaction = supportFragmentManager.beginTransaction()

        if (isAnimated) {
            setAnimation(transaction)
        }

        transaction.replace(getFragmentContainerId(), fragment, fragment.javaClass.simpleName)

        if (backStackTag != null) {
            transaction.addToBackStack(backStackTag)
        }

        return transaction
    }

    open fun setAnimation(fragmentTransaction: FragmentTransaction) {
        fragmentTransaction.setCustomAnimations(0, 0, 0, 0)
    }

    fun showToast(messageResource: Int) {
        Toast.makeText(this, getString(messageResource), Toast.LENGTH_SHORT).show()
    }

    fun popBackStack() = supportFragmentManager.popBackStack()

    fun showLoadingDialog() {
        if (!isLoadingDialogDisplaying) {
            show(loadingDialog)
            isLoadingDialogDisplaying = true
        }
    }

    fun hideLoadingDialog() {
        if (isLoadingDialogDisplaying) {
            supportFragmentManager.beginTransaction().remove(loadingDialog).commit()
            isLoadingDialogDisplaying = false
        }
    }

    fun show(fragment: Fragment, backStackTag: String?, isAnimated: Boolean) {
        buildTransaction(fragment, backStackTag, isAnimated)
            .commit()
    }

    private fun show(dialogFragment: DialogFragment) {
        dialogFragment.show(supportFragmentManager, dialogFragment::class.java.simpleName)
    }
}