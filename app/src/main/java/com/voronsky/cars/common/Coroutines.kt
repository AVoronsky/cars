package com.voronsky.cars.common

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import timber.log.Timber
import kotlin.coroutines.CoroutineContext

class CoroutinesDispatcher(private val io: CoroutineContext = Dispatchers.IO) {

    fun io(
        work: suspend (() -> Unit),
        onError: (Throwable) -> Unit,
        onComplete: () -> Unit = {}
    ): Job =
        CoroutineScope(io).launch {
            try {
                work()
            } catch (e: Exception) {
                Timber.e(e, "An error occurred in IO thread")
                onError(e)
            } finally {
                onComplete()
            }
        }
}

