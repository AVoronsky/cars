package com.voronsky.cars.common.base

import android.content.Context
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import dagger.android.support.DaggerFragment

abstract class BaseFragment : DaggerFragment() {

    private val safeActivity: FragmentActivity
        get() {
            return activity ?: throw NullPointerException("Activity is null!")
        }

    val safeContext: Context
        get() {
            return context ?: throw NullPointerException("Context is null!")
        }

    fun showToast(messageResource: Int) {
        getBaseActivity().showToast(messageResource)
    }

    protected fun getLoadingObserver(): Observer<Boolean> {
        return Observer {
            if (it) {
                getBaseActivity().showLoadingDialog()
            } else {
                getBaseActivity().hideLoadingDialog()
            }
        }
    }

    private fun getBaseActivity(): BaseActivity = safeActivity as BaseActivity
}