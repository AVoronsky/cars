package com.voronsky.cars.common

/**
 * An object to pass some data or exception via LiveData.
 * Designed to use a single object as DTO and exclude lots of LiveDatas for different sates
 *
 * @param data - data to pass via this DTO, if no data to pass, e.g. from Completable, just put Any()
 * @param throwable - if error is present and has to be pushed to UI, put its throwable, otherwise null
 */
class VMDataWrapper<out T>(val data: T? = null, val throwable: Throwable? = null)