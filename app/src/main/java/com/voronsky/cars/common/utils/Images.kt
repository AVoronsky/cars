package com.voronsky.cars.common.utils

import android.widget.ImageView
import androidx.core.content.ContextCompat
import com.voronsky.cars.GlideApp
import com.voronsky.cars.R

fun ImageView.loadImage(url: String) {
    GlideApp.with(this.context)
        .load(url)
        .error(ContextCompat.getDrawable(this.context, R.mipmap.ic_launcher_round))
        .into(this)
}