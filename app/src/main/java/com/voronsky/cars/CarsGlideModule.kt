package com.voronsky.cars

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

@GlideModule
class CarsGlideModule : AppGlideModule()