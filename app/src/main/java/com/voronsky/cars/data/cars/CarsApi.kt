package com.voronsky.cars.data.cars

import retrofit2.http.GET

interface CarsApi {

    @GET("/codingtask/cars")
    suspend fun getCars(): List<CarDto>
}