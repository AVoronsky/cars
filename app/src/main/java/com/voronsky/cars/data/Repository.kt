package com.voronsky.cars.data

import com.voronsky.cars.data.cars.CarsRepository

interface Repository {

    fun getCarsRepository(): CarsRepository
}

class DataRepository(private val carsRepository: CarsRepository) : Repository {
    override fun getCarsRepository(): CarsRepository {
        return carsRepository
    }
}