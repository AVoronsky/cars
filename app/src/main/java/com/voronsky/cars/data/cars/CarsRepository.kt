package com.voronsky.cars.data.cars

interface CarsRepository {

    /**
     * Retrieves cars from the server
     *
     * @return - returns a cars list
     */
    suspend fun getCars(): List<CarDto>
}

class CarsRestRepository(private val carsApi: CarsApi) : CarsRepository {

    override suspend fun getCars() = carsApi.getCars()
}