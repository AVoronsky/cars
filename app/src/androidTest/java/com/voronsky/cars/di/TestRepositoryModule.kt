package com.voronsky.cars.di

import com.voronsky.cars.common.network.RestNetworking
import com.voronsky.cars.data.DataRepository
import com.voronsky.cars.data.Repository
import com.voronsky.cars.data.cars.CarDto
import com.voronsky.cars.data.cars.CarsApi
import com.voronsky.cars.data.cars.CarsRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class TestRepositoryModule {

    @Provides
    @Singleton
    fun providesCarsApi(restNetworking: RestNetworking): CarsApi {
        return restNetworking.create(CarsApi::class.java)
    }

    @Provides
    @Singleton
    fun providesCarsRepository(): CarsRepository {
        return TestCarsRepository()
    }

    @Provides
    @Singleton
    fun providesRepository(carsRepository: CarsRepository): Repository {
        return DataRepository(carsRepository)
    }
}

class TestCarsRepository : CarsRepository {
    override suspend fun getCars(): List<CarDto> {
        val cars = mutableListOf<CarDto>()
        for (i in 0..10) {
            val dto = CarDto(
                "id$i", "model$i", "modelName$i", "name$i", "make$i", "group$i",
                "color$i", "series$i", "fuel$i", i.toDouble(), "transm$i", "license$i",
                i.toDouble(), i.toDouble(), "", ""
            )
            cars.add(dto)
        }
        return cars
    }
}