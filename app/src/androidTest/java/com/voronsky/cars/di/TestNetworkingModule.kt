package com.voronsky.cars.di

import com.google.gson.Gson
import com.voronsky.cars.BuildConfig
import com.voronsky.cars.common.network.Connection
import com.voronsky.cars.common.network.RestNetworking
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class TestNetworkingModule {
    @Provides
    @Singleton
    fun provideGson() = Gson()

    @Provides
    @Singleton
    fun provideConnection(): Connection {
        return object : Connection {
            override fun isNetworkConnected(): Boolean {
                return true
            }

        }
    }

    @Provides
    @Singleton
    fun provideRestNetworking(gson: Gson): RestNetworking {
        return RestNetworking(BuildConfig.BASE_API_URL, gson)
    }
}