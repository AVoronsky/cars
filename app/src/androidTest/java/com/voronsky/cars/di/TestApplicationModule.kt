package com.voronsky.cars.di

import android.content.Context
import com.voronsky.cars.TestCarsApplication
import com.voronsky.cars.common.CoroutinesDispatcher
import com.voronsky.cars.screens.main.MainActivity
import com.voronsky.cars.screens.main.di.TestMainActivityModule
import dagger.Module
import dagger.Provides
import dagger.android.AndroidInjectionModule
import dagger.android.ContributesAndroidInjector
import javax.inject.Singleton

@Module(includes = [AndroidInjectionModule::class])
abstract class TestApplicationModule {

    @Module
    companion object {
        @JvmStatic
        @Provides
        @Singleton
        fun provideApplicationContext(testCarsApplication: TestCarsApplication)
                : Context = testCarsApplication

        @JvmStatic
        @Provides
        @Singleton
        fun coroutines() = CoroutinesDispatcher()
    }

    @ActivityScope
    @ContributesAndroidInjector(modules = [TestMainActivityModule::class])
    abstract fun bindMainActivity(): MainActivity
}