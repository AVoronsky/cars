package com.voronsky.cars

import com.voronsky.cars.di.TestApplicationModule
import com.voronsky.cars.di.TestNetworkingModule
import com.voronsky.cars.di.TestRepositoryModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        TestApplicationModule::class,
        TestNetworkingModule::class,
        TestRepositoryModule::class
    ]
)
interface TestApplicationComponent : AndroidInjector<TestCarsApplication> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun app(testCarsApplication: TestCarsApplication): Builder

        fun build(): TestApplicationComponent
    }
}