package com.voronsky.cars.screens.cars

import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import com.voronsky.cars.R
import com.voronsky.cars.runner.CarsJUnitRunner
import com.voronsky.cars.screens.cars.adapter.CarsAdapter
import com.voronsky.cars.screens.main.MainActivity
import org.junit.Assert
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class CarsListFragmentTest {

    @Rule
    @JvmField
    val rule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun recyclerViewsShowedItems() {
        openCarsListFragment()
        onView(withId(R.id.rvCars)).check(matches(isDisplayed()))

        Assert.assertTrue(getRecyclerView().adapter!!.itemCount > 0)
    }

    @Test
    fun scrollListToLastItem() {
        openCarsListFragment()
        val lastPosition = getRecyclerView().adapter!!.itemCount - 1
        onView(withId(R.id.rvCars))
            .perform(RecyclerViewActions.scrollToPosition<CarsAdapter.ViewHolder>(lastPosition))
    }

    @Test
    fun clickEveryItemInList() {
        openCarsListFragment()
        val rv = getRecyclerView()
        for (i in 0 until rv.adapter!!.itemCount) {
            onView(withId(R.id.rvCars))
                .perform(RecyclerViewActions.scrollToPosition<CarsAdapter.ViewHolder>(i))
                .perform(
                    RecyclerViewActions
                        .actionOnItemAtPosition<CarsAdapter.ViewHolder>(i, click())
                )
        }
    }

    private fun openCarsListFragment() {
        onView(withId(R.id.btnCarsList)).perform(click())
    }

    private fun getRecyclerView(): RecyclerView {
        val fragment = rule.activity.supportFragmentManager
            .findFragmentByTag(CarsListFragment::class.java.simpleName)
        return fragment!!.view!!.findViewById(R.id.rvCars)
    }
}