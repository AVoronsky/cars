package com.voronsky.cars.screens.dispatch

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import com.voronsky.cars.R
import com.voronsky.cars.screens.cars.CarsListFragment
import com.voronsky.cars.screens.carsMap.CarsMapFragment
import com.voronsky.cars.screens.main.MainActivity
import org.junit.Assert
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class DispatchFragmentTest {

    @Rule
    @JvmField
    val rule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun buttonsHaveACorrectSetup() {
        onView(withId(R.id.btnCarsList))
            .check(matches(withText(R.string.dispatcher_show_cars_list)))
            .check(matches(isClickable()))
        onView(withId(R.id.btnCarsMap))
            .check(matches(withText(R.string.dispatcher_show_cars_map)))
            .check(matches(isClickable()))
    }

    @Test
    fun showCarsListButtonOpensNeededFragment() {
        onView(withId(R.id.btnCarsList))
            .perform(click())

        val fragments = rule.activity.supportFragmentManager.fragments
        Assert.assertEquals(fragments[0].tag, CarsListFragment::class.java.simpleName)
    }

    @Test
    fun showCarsMapButtonOpensNeededFragment() {
        onView(withId(R.id.btnCarsMap))
            .perform(click())

        val fragments = rule.activity.supportFragmentManager.fragments
        Assert.assertEquals(fragments[0].tag, CarsMapFragment::class.java.simpleName)
    }
}