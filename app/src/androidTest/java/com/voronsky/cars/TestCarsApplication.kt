package com.voronsky.cars

import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

class TestCarsApplication : DaggerApplication() {
    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        val component = DaggerTestApplicationComponent
            .builder()
            .app(this)
            .build()
        component.inject(this)
        return component
    }
}